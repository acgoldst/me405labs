## @file propasal.py
#  Brief doc for proposal.py
#
# Detailed doc for proposal.py
#
# @page page_prop Proposal Ref
#
##  @section sec_prop Proposal Request
#
# Author: Adam Goldstein
#
#Problem Statement: For the ME 405 Term Project, I would like to build a lane autonomous car with lane detection capability. The vehicle must be to recognize a line placed on the ground such as a length of tape and be able to track it for an extended length. For this requirement, the track will be between 50 and 100ft long and included many turns with varying amounts of curvature. The purpose of this project is to get a better understanding of computer vision technology and gain a glimpse into the field of autonomous vehicle lane detection technology. 
#
## @section sec_requir Project Requirement Compliance:  
#This autonomous vehicle will include two DC motors to power the front wheels. The turning of the car will be done with a closed loop control system connected to the motors which will vary the PWM amount to each motor based on the amount the vehicle needs to rotate when going around a turn. 
#
#The included encoder will be used to keep track of the distance and angular velocity of the motor which will be send back to the control system.
#
#The IMU will be attached as well to record the orientation and direction of the car
#
#An LCD panel may be purchased to output the vehicle dynamic characteristics in real time while the vehicle is moving.
#
#For the lane detection sensor, a Pixycam will be purchased to supply the computer vision lane detection capability.  Although this device is expensive, I will hope to be able to reuse it in ME 507. 
#All of this hardware will run off of the Nucleo Board. 
#
## @section sec_mat Project Material Choice:
#
##Wheels: Two sets of Pololu Wheel 70x8mm red: $4.75x2 (pololu.com)
#
#Gear Motor: Pololu Micro Metal Gear Motor Bracket Pair Black: $4.99 (pololu.com)
#
#Chassis: Balboa Chassis with Stability Conversion Kit: $32 (pololu.com)
#
#Robot Vision Sensor: Pixy2 Smart Sensor: $60 (amazon)
#
#LCD Panel: RIO Rand LCD Module: $8 (Amazon)
#
#ADA Fruit battery mounting kit: $2 (Mouser Electronics)
#
#Included ME 405 Components: 
#DC Step Motor with encoder x2
#STM32 Microcontroller
#Motor Driver
#IMU
#
## @section sec_man Manufacturing Plan: 
#
#Motors and wheels will be screwed into Chassis
#
#Microcontroller will be attached to top of chassis by either screws or adhesive
#
#Pixycam will be attached to front of chassis by screws
#
#May need to go to local hardware store during manufacturing process
#
## @section sec_haz Hazard Assessment:
#
#Project is low power but will need batteries to power motor
#
#Pinch Point Risk is low
#
#Overheating risk is low as it is a small product under low power
#
## @section sec_time Project Timeline:
#
#Order Parts Sunday May 24th 
#
#Sunday May 24th Begin writing and testing software for motor control system
#
#Parts arrive by Thursday May 28th 
#
#Begin assembling following weekend of May 30th 
#
#Testing weekend of May 30th
#
#Following two weeks will continue debugging and frequent communication with instructor to ensure project is functioning by June 12th 
