## @file Controller.py
#  Brief doc for Controller File
#
#  Detailed doc for Controller.py
#
# @author Adam Goldstein
#
# https://acgoldst@bitbucket.org/acgoldst/me405labs.git
#
#  Details

from Encoder.py import Encoder
import pyb
import utime

class Controller:
    ## Constructor for Controller
    #
    #  detailed info on Controller driver
    # Controller takes gain input and then sends actuation signal to
    # motor as duty cycle

    def __init__ (self, Kp):
        """ Initializes controller variables
        as the input gain Kp"""
        self.Kp = Kp
        ## Updates the encoder
        # takes in the position of the encoder
        # records the new position and based on the delta value
        # Will correct for overflow or underflow error



    def update(self, setpoint, measured):
        """ Updates has setpoint and measured as arguments
        then returns an actuation value based on the difference between
        the measured value taken from encoder and the setpoint multiplied
        by the gain"""
        ## Gets position of encoder
         # returns the update method

        err = setpoint - measured
        actuation = err*Kp
        return actuation



