## @file main.py
#  Brief doc for main file
#
#  Detailed doc for main.py
#
# @author Adam Goldstein
#
# https://acgoldst@bitbucket.org/acgoldst/me405labs.git
#
#  Details

from Motor import MotorDriver
from Encoder import Encoder
from Controller import Controller
import pyb
import utime



if __name__ == '__main__':
    ## Enables pins A10, B4, and B5 on motor board to control motor
    pin_EN  = pyb.Pin.cpu.A10
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5

    #Enables timer
    tim3 = pyb.Timer(3, freq = 20000)

    # Creates motor object
    moe  = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim3)

    #Enables motor
    moe.enable()
    
    # Encables pins for encoder and timer for encoder
    pin_PB6 = pyb.Pin.cpu.B6 
    pin_PB7 = pyb.Pin.cpu.B7
    timer4 = pyb.Timer(4, prescaler=0, period=65352)
    # Creates encoder object
    encoder1 = Encoder(pin_PB6, pin_PB7, timer4)

    # While loop that waits for user input of gain value
    while True:        
        encoder1.set_position(1)
        setpoint = 1000 #sets position setpoint to be 1000
        Kp = input('Please enter a gain value or type q to quit: ')  # % Duty cycle/encoder count
        if Kp == 'q':                                           # checks if q is typed to quit
            break
        Kp = float(Kp)
        controller1 = Controller(Kp)                     # creates controller object
        times = []
        positions = []
        ticks0 = utime.ticks_ms()
        for i in range(200):                     # Completes 200 iterations to collected encoder position value
            utime.sleep_ms(10)
            measured = -encoder1.get_position()
            actuation = controller1.update(setpoint, measured)
            moe.set_duty(actuation)
            time = utime.ticks_diff(utime.ticks_ms(),ticks0)
            times.append(time)
            positions.append(measured)
        moe.set_duty(0)
        for n in range(200):                       # Prints in collumn format
            print('{:},{:}'.format(utime.ticks_diff(times[n],times[0]),positions[n]))


