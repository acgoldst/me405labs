## @file Motor.py
#  Brief doc for Motor Driver
#
#  Detailed doc for Motor.py
#
# @author Adam Goldstein
#
# https://acgoldst@bitbucket.org/acgoldst/me405labs.git
#

#  Details
import pyb

class MotorDriver:
    ## Constructor for Motor driver
    #
    #  This class contains the code to control the motor
    # This include forward and reverse functionality from 0-100% pwm
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        """ Initializes motor variables
        Constructor initializes the motor arguments provided as well as
        initiates a timer variable for channel 1 and channel 2 on the motor driver"""
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.t3ch1 = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.t3ch2 = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
        print ('Creating a motor driver')


    ## Enables the motor
    # Sets the EN1 pin to high to power motor
    def enable (self):
        self.EN_pin.high()
        print ('Enabling Motor')

    ## Disables the motor
    # Sets the EN1 pin to low to hold motor
    def disable (self):
        self.t3ch1.pulse_width_percent(0)
        self.t3ch2.pulse_width_percent(0)
        print ('Disabling Motor')

    ## Controls the duty cycle and direction of rotation by setting IN1 to low
    # Then sets a timer with IN2 to control PWM based on the input duty value provided
    # Can operate in reverse direction by doing the opposite
    def set_duty (self, duty):
        if duty >= 0 and duty <= 100:
            self.t3ch1.pulse_width_percent(0)  # Sets IN1 to low for forward mode and adjusts IN2 for PWM
            self.t3ch2.pulse_width_percent(duty)
        elif duty<0 and duty >=-100:
            self.t3ch2.pulse_width_percent(0) # Sets IN2 to low for reverse mode and adjusts IN1 for PWM
            self.t3ch1.pulse_width_percent(-duty)


if __name__ == '__main__':

    ## Enables pins A10, B4, and B5 on motor board to control motor
    pin_EN  = pyb.Pin.cpu.A10
    pin_IN1 = pyb.Pin.cpu.B4
    pin_IN2 = pyb.Pin.cpu.B5

    #Enables timer
    tim3 = pyb.Timer(3, freq = 20000)
    

    # Creates motor object
    moe  = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim3)

    #Enables motor
    moe.enable()

    # Sets duty cycle
    moe.set_duty(10)
    moe.set_duty(50)
    moe.set_duty(-10)
    moe.set_duty(0)
    moe.set_duty(20)
    moe.set_duty(-30)
    moe.set_duty(0)
    
    #moe.disable()
    

