## @file mainpage.py
# @author Adam Goldstein
# @mainpage
# @section intro Introduction
# Some information on the whole project
#
# This project was created for the Cal Poly Mechtronics (ME 405) Class
#
# This projects combines a set of labs completed throughout the quarter all building on each other
#
# Author: Adam Goldstein
#
#  @section sec_mot Motor Driver
#  Some information about the motor driver with links. Please see motor.Motor which is part of the \ref motor package.
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab1/Motor.py
#
# Purpose: To be able to set a motor power output based on a duty cycle input from user
# Also initializes a motor and pins when creating the motor object
#
# Usage: Used to run a motor at varying duty cycles. Creates motor object specifc to pins motor is conencted to on board
#
# Testing: Can be testing by user with motor, tested motor at varying PWM values as well as using negative values
# To change direction while motor is moving. Have set duty cycle to zero to stop motor as well as turn off pin EN1
#
# Bugs and Limitations: Have not tested two motors together yet
#
#  @section sec_end Encoder Driver
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab2/Encoder.py
#
# Purpose: To Keep track of the position of the motor and to be able to read the velocity of a motor
#
# Usage: Assume correct encoder pins are connected to STM32 and to be used with the timer module in pyb, creates Encoder object 
#
# Testing: Can be tested using multiple motors connected
#
# Bugs and Limitations: Encoder only tested on timer 4 and timer 8, its possible that there could be bugs on other times
# Have not tested other pins on board besides PB6,7 and PC6,7
#
#  @section sec_cont Controller Driver
#  Some information about the Controller driver with links. 
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab3/Controller.py
#
# Purpose: To allow the motor to approach a position by slowing itself down as it gets closer
#
# Usage: User inputs a gain value and initializes a step motor as well as initializes an encoder properly.
# Assumes gain inputs are incorrect float format.
#
# Testing: Was tested on one motor with setpoint at 1000  
#
# Bugs and Limitations: May not work on multiple motors simulataneously
#
##  @section sec_control Controller Tuning
#  Description for tuning process of controller 
#
# The controller was tuned by running step responses ranging from Kp = .1 - Kp .01
#
# In doing so it was determined that the as the controller gain increased, the quicker
# the repsonse would reach its setpoint target of 1000. With Kp =.01, the gain was too low for
# the controller to recognize it so the motor never turned. At Kp = 1, the motor sped up quick enough
# although it over shot the target by a significant margin. For this reason, a Kp rangin from around .05 to
# .1 resulted in a resonable peak time.
#
## Step Response Plots
# Step Response Plots
#
# @image html kp03.png
#
# As position approaches 1000, there is a slight reversal in position from the controller over correcting itself with too high
# of a gain
#
# @image html kp01.png
#
# As position approaches 1000, the controller smoothly slows down the velocity of the motor to 0 duty cycle
#
#
# @image html kp005.png
#
# The controller here takes slight longer to approaches its peak value as gain is not high enough
#
## Controller Video
#
# Link to Video of Controller running at a gain of Kp = 0.1
#
# https://www.youtube.com/watch?v=hn0_TBAFhqw
