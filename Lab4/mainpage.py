## @file mainpage.py
# @author Adam Goldstein
# @mainpage
# @section intro Introduction
# Some information on the whole project
#
# This project was created for the Cal Poly Mechtronics (ME 405) Class
#
# This projects combines a set of labs completed throughout the quarter all building on each other
#
# Author: Adam Goldstein
#
#  @section sec_mot Motor Driver
#  Some information about the motor driver with links. Please see motor.Motor which is part of the \ref motor package.
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab1/Motor.py
#
# Purpose: To be able to set a motor power output based on a duty cycle input from user
# Also initializes a motor and pins when creating the motor object
#
# Usage: Used to run a motor at varying duty cycles. Creates motor object specifc to pins motor is conencted to on board
#
# Testing: Can be testing by user with motor, tested motor at varying PWM values as well as using negative values
# To change direction while motor is moving. Have set duty cycle to zero to stop motor as well as turn off pin EN1
#
# Bugs and Limitations: Have not tested two motors together yet
#
#  @section sec_end Encoder Driver
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab2/Encoder.py
#
# Purpose: To Keep track of the position of the motor and to be able to read the velocity of a motor
#
# Usage: Assume correct encoder pins are connected to STM32 and to be used with the timer module in pyb, creates Encoder object 
#
# Testing: Can be tested using multiple motors connected
#
# Bugs and Limitations: Encoder only tested on timer 4 and timer 8, its possible that there could be bugs on other times
# Have not tested other pins on board besides PB6,7 and PC6,7
#
#  @section sec_cont Controller Driver
#  Some information about the Controller driver with links. 
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab3/Controller.py
#
# Purpose: To allow the motor to approach a position by slowing itself down as it gets closer
#
# Usage: User inputs a gain value and initializes a step motor as well as initializes an encoder properly.
# Assumes gain inputs are incorrect float format.
#
# Testing: Was tested on one motor with setpoint at 1000  
#
# Bugs and Limitations: May not work on multiple motors simulataneously
#
#  @section sec_imu IMU Driver
#  Some information about the IMU Code. 
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab4/IMU.py
#
# Purpose: The inertial Measurement Unit uses sensors to determine orientation with respect to earth.
# This allows for measuremnts of velocity, acceleration and angle
#
# Usage: User runs main script which continiously checks the orientation and velocity of the system every 1 second
# To ensure the correct orientation is done, the user must calibratate the IMU by continously rotating it to ensure
# each outputted orientation lines up with the users expected orientation between (0-360) degrees.
# Running this device assumes user connected correct pins from IMU to STM32 microcontroller
# 
# Testing: Testing was done by rotating the imu in 90 degree increments until the angles outputted on the shell
# Matched up with the expected values. Calibration is still limitted and the protractor method is not highly accurate as there
# are human precision errors to be expected
#
# Bugs and Limitations: Have not tested a slave driver yet. The velocity values have not been properly calibrated either yet.
