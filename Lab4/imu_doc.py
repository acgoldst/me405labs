## @file imu_doc.py
#  Brief doc for imu_doc.py
#
# Detailed doc for imu_doc.py
#
# @page page_imu IMU Reference
#
##  @section sec_imu IMU Calibration
#  Description for calibration process of IMU
#
# Testing: Testing was done by rotating the imu in 90 degree increments until the angles outputted on the shell
# Matched up with the expected values. Calibration is still limitted and the protractor method is not highly accurate as there
# are human precision errors to be expected
#
# The video below shows a short clip of the calibration process. Here it is seen that the IMU
# is rotated in 90 degree increments to allow the output on the screen to show changes in orientation by 90
# degrees
#
# Youtube Link: https://youtu.be/UEz_K5JMHN8
