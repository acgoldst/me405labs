## @file con_doc.py
# Brief doc for con_doc.py
#
# Detailed Doc for con_doc.py
#
# @page page_con Controller Reference
#
##  @section sec_control Controller Tuning
#  Description for tuning process of controller 
#
# The controller was tuned by running step responses ranging from Kp = .1 - Kp .01
#
# In doing so it was determined that the as the controller gain increased, the quicker
# the repsonse would reach its setpoint target of 1000. With Kp =.01, the gain was too low for
# the controller to recognize it so the motor never turned. At Kp = 1, the motor sped up quick enough
# although it over shot the target by a significant margin. For this reason, a Kp rangin from around .05 to
# .1 resulted in a resonable peak time.
#
## Step Response Plots
# Step Response Plots
#
# @image html kp03.png
#
# As position approaches 1000, there is a slight reversal in position from the controller over correcting itself with too high
# of a gain
#
# @image html kp01.png
#
# As position approaches 1000, the controller smoothly slows down the velocity of the motor to 0 duty cycle
#
#
# @image html kp005.png
#
# The controller here takes slight longer to approaches its peak value as gain is not high enough
#
## Controller Video
#
# Link to Video of Controller running at a gain of Kp = 0.1
#
# https://www.youtube.com/watch?v=hn0_TBAFhqw
