## @file IMU.py
#  Brief doc for IMU Sensor
#
#  Detailed doc for IMU.py
#
# @author Adam Goldstein
#
# https://acgoldst@bitbucket.org/acgoldst/me405labs.git
#
#
#  Details
import pyb
from pyb import I2C
from micropython import const
import ustruct
import utime

ANG_X_LSB = const(0x1a)
GYR_X_LSB = const(0x14)
OP_Mode = const(0x3d)
Config_Mode = const(0x00)
NDOF_Mode = const(0x0c)
Normal_Power = const(0x00)
PWR_Mode = const(0x3e)
CALIB_STAT = const(0x35)

class ImuCom:
    ## Constructor for IMU Communicator
    #
    #  This class contains the code to communucate with the IMU board from the STM32 MicroController
    # Class alows user to get a veloctity orientation and check the calibration status of the imu
    # As well as enabkle and disable the device
    def __init__ (self):
        """ Initializes motor variables
        Constructor initializes IMU by setting it to master mode on bus one and having it accessable
        throughout the class"""
        self.i2c = I2C(1,I2C.MASTER)

    def enable(self):
        """ Enables the IMU by setting it to NDOF_Mode (Fusion 9 axis) in memory)
        This occurs at memory address 0x3d in in memory where the value is overwritted to 0x3d"""
        self.i2c.mem_write(NDOF_Mode, 0x28, OP_Mode) 
                                            
    def disable(self):
        """ Method disables the IMU by calling the I2C deinit() function"""
        self.i2c.deinit()

    def set_mode(self, Operation_Code):
        """ Set the IMU mode at memory address 0x3d to whatever accelerometer mode is specified""" 
        self.i2c.mem_write(Operation_Code, 40, OP_Mode) 
                                           
    def check_cal_status(self):
        """ Class checks the if the IMU is properly calibrated
        will output 3 for all calibration sensors if properly calibrated and 0 if
        not calibrated at all.
        Process takes the Calibration status register and bit shifts each two bit calibration value
        to the far right, then masks it to only show its value to be outputted between dec 0-3"""
        data = self.i2c.mem_read(1, 40, CALIB_STAT)
        sys_cal = (data[0] >> 6) & 0b11
        gyr_cal = (data[0] >> 4) & 0b11
        acc_cal = (data[0] >> 2) & 0b11
        mag_cal = (data[0]) & 0b11
        return (sys_cal, gyr_cal, acc_cal, mag_cal)
        
    def get_orientation(self, DEV_ADDR, EUL_Heading_LSB):
        """ Method gets the orientation of the IMU by outputting each value
        stored in memory registers starting at at the EUL_X LSB register and following 6
        memory addresses below that for x, y, z. This value is then scaled to be returned as a value between 0-360 degrees
        returned as a tuple"""
        data = self.i2c.mem_read(6, DEV_ADDR, EUL_Heading_LSB)  # read six bytes representing the LSB and MSB from x, y, and z data
        values = ustruct.unpack('<hhh',data)               # unpack the data into 3 signed integers
        a = values[0]
        a = a*(360/5800)
        b = values[1]*(360/1500)
        c = values[2]*(360/2900)
        return a,b,c


    def get_anglular_velocity(self, DEV_ADDR, GYR_Data_LSB):
        """ function takes the device address of IMU and the first velcocity register value
        and unpacks it and the following five regsiter values below that in memory to
        return the calibrated velocity of the system as a tuple"""
        data = self.i2c.mem_read(6, DEV_ADDR, GYR_Data_LSB)  # read six bytes representing the LSB and MSB from x, y, and z data
        values = ustruct.unpack('<hhh',data)               # unpack the data into 3 signed integers
        return values


if __name__ == '__main__':
    """ Driver code"""

    imu = ImuCom() # Created imu objected
    imu.enable()   # Enables imu
    
    # Queries imu at 1 second intervals to return the calibrations status, the orientation and angular velocity
    while True:
        utime.sleep(1)
        Eul_ang = imu.get_orientation(0x28, 0x1a)
        Eul_vel = imu.get_anglular_velocity(0x28, GYR_X_LSB)
        print('Eul Ang: ', Eul_ang)
        print('Eul Vel: ', Eul_vel)
        print('Calib: ', imu.check_cal_status())

    
