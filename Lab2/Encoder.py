## @file Encoder.py
#  Brief doc for Incremental Encoder File
#
#  Detailed doc for Encoder.py
#
# @author Adam Goldstein
#
# https://acgoldst@bitbucket.org/acgoldst/me405labs.git
#
#  Details


import pyb # imports the pyb module
import utime # imports the utime module

class Encoder:
    ## Constructor for Encoder
    #
    #  Detailed info on encoder driver constructor
    # Encoder file assumes user inputs correct pins 
    # When initializes the timer for the encoder
    # As the timer is already initialzied when passed in
    # To create the encoder object
    # When initializing the timer, assume prescaler is set to 0
    # and period = 65352 which is greatest decimal value to represent 
    # by 16 bit value as thats the most the encoder can record

    def __init__ (self, pinA, pinB, timer):
        """ Initializes encoder variables
        attributes are pinA and pinB for encoder
        as well as timer variable which is already initialized
        Constructor initializes timers channel 1 and 2 for encoder object
        Constructor sets a position and delta value that will be accessible throughout
        the class"""
        
        self.pinA = pinA
        self.pinB = pinB
        self.timer = timer
        self.timch1 = timer.channel(1, pyb.Timer.ENC_AB, pin=pinA)
        self.timch2 = timer.channel(2, pyb.Timer.ENC_AB, pin=pinB)
        self.pos = 0
        self.delta = 0


    ## Updates the encoder
    # takes in the position of the encoder
    # records the new position and based on the delta value
     # Will correct for overflow or underflow error
    def update(self):
        self.delta = 0
        num_line = 65352
        prev_count = self.pos  # Sets previous counter to positional value
        utime.sleep_ms(200)         # Sets delay of 20ms before calling
        cur_count = self.timer.counter()    # Takes current count value
        self.delta = cur_count - prev_count   # Takes differences between prev and curr
        if self.delta > .5*num_line: # Underflow has occured
            self.delta -= num_line
        if self.delta < -.5*num_line: # Overflow has occured
            self.delta += num_line
        self.pos = prev_count + self.delta # Sets the new position to be the current value plus the updated delta difference
        return self.pos

    ## Gets position of encoder
    # returns the update method
    def get_position(self):
        
        return self.update()        # get counter (can also set)

    ## Sets the encoder position
    def set_position(self, pos):
        self.timer.counter(pos)
        self.pos = self.timer.counter()

    ## Gets the delta value for the encoder
    # This is the difference between the previous recorded value and the current value
    # which varies based on if the encoder has completed overflow or underflow
    def get_delta(self):
        return self.delta
        
if __name__ == '__main__':


    pin_PB6 = pyb.Pin.cpu.B6
    pin_PB7 = pyb.Pin.cpu.B7
    pin_PC6 = pyb.Pin.cpu.C6
    pin_PC7 = pyb.Pin.cpu.C7


    timer4 = pyb.Timer(4, prescaler=0, period=65352)
    

    timer8 = pyb.Timer(8, prescaler=0, period=65352)

    encoder1 = Encoder(pin_PB6, pin_PB7, timer4)
    encoder2 = Encoder(pin_PC6, pin_PC7, timer8)
    

    #encoder.set_position(65340)
    
    while True:
        print(encoder1.update())
        print(encoder2.update())

    #print(encoder.update())
    #print(encoder.update())
        