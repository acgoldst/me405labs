## @file mainpage.py
# @author Adam Goldstein
# @mainpage
# @section intro Introduction
# Some information on the whole project
#
# This project was created for the Cal Poly Mechtronics (ME 405) Class
#
# This projects combines a set of labs completed throughout the quarter all building on each other
#
# Author: Adam Goldstein
#
#  @section sec_mot Motor Driver
#  Some information about the motor driver with links. Please see motor.Motor which is part of the \ref motor package.
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab1/Motor.py
#
# Purpose: To be able to set a motor power output based on a duty cycle input from user
# Also initializes a motor and pins when creating the motor object
#
# Usage: Used to run a motor at varying duty cycles. Creates motor object specifc to pins motor is conencted to on board
#
# Testing: Can be testing by user with motor, tested motor at varying PWM values as well as using negative values
# To change direction while motor is moving. Have set duty cycle to zero to stop motor as well as turn off pin EN1
#
# Bugs and Limitations: Have not tested two motors together yet
#
#  @section sec_end Encoder Driver
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
# Location: https://bitbucket.org/acgoldst/me405labs/src/master/Lab2/Encoder.py
#
# Purpose: To Keep track of the position of the motor and to be able to read the velocity of a motor
#
# Usage: Assume correct encoder pins are connected to STM32 and to be used with the timer module in pyb, creates Encoder object 
#
# Testing: Can be tested using multiple motors connected
#
# Bugs and Limitations: Encoder only tested on timer 4 and timer 8, its possible that there could be bugs on other times
# Have not tested other pins on board besides PB6,7 and PC6,7
#
