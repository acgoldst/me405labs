''' @file main.py
There must be a docstring at the beginning of a Python source file
with an @file [filename] tag in it! '''

"""@file main.py
 File prompts user input for integer >= 0 and returns the
fibbanacci value associated with that input"""



def fib (idx):
    ''' This method calculates a Fibonacci number corresponding to
    a specified index.
    @param idx An integer specifying the index of the desired
    Fibonacci number.'''

    if (idx == 1):              # Checks if input is 1 then returns 1
        return 1
    elif (idx == 0):            # IF input is zero, returns 0
        return 0
    else:
        return fib(idx-1) + fib(idx-2)      # Else returns the sum of idx -1 and idx -2

                    
              

if __name__ =='__main__':

    # Replace the following statement with the user interface code
    # that will allow testing of your Fibonnaci function. Any code
    # within the if __name__ == '__main__' block will only run when
    # the script is executed as a standalone program. If the script
    # is imported as a module the code block will not run.

    idx = input('Please enter an index: ')
    while True:
        
        if idx == 'q':                      # If the letter q is typed, then program will quit
            break
        try:
            idx = int(idx)                  # Checks if an typed value is an integer
            if idx >= 0:
                print ('Calculating Fibonacci number at ' 'index n = {:}.'.format(idx)) 
                print(fib(idx))             
            else:
                print("Error, Invalid Index")
        except ValueError:
            print("Error, Invalid Index")
        idx = input('Please enter another index or type q to quit: ')   # If invalid index, Error message reported

    
    